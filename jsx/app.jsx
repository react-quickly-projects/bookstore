const React = require('react')
const ReactDOM = require('react-dom')
const {
    Router,
    Route,
    Link,
    IndexRoute,
    hashHistory
} = require('react-router')
const Product = require('./product/product-link.jsx')
const ProductView = require('./product/product-view.jsx')
const Cart = require('./cart.jsx')

const PRODUCTS = [
    {
        id: 0,
        src: 'images/reactquickly-cover.jpg',
        title: 'React Quickly'
    },
    {
        id: 1,
        src: 'images/practicalnode-cover.jpeg',
        title: 'Practical Node'
    }
]

const cart = {}

const Heading = () => (<h1>Christian's Book Store</h1>)
const Copy = () => (<p>Please click on a book to view details in a modal. You can copy/paste the link of the modal.</p>)

const handleBuy = function(id) {
    addToCart(id)
}

const addToCart = function(id) {
    if(cart[id])
        cart[id]++
    else
        cart[id] = 1
}

class App extends React.Component {
    render() {
        return (
            <div>
                <nav className='navbar navbar-dark bg-dark'>
                    <h1 style={{ color: 'white' }}>App nav</h1>
                    <Link to='/home' className='nav-link'>Home</Link>
                    <Link to='/cart' className='nav-link'>Test</Link>
                </nav>
                {this.props.children}
            </div>
        )
    }
}

const Index = function (props) {
    return (
        <div>
            <Heading />
            <Copy />
            <Link to='/cart' className='btn btn-primary'>Cart</Link>
            <div className='product-container'>
                {PRODUCTS.map((product, index) => (<Product key={index} {...product} />))}
            </div>
        </div>
    )
}

const Test = function() {
    return <h1>Test</h1>
}

ReactDOM.render(
    // <Index/>
    <Router history={hashHistory}>
        <Route path='/' component={App}>
            <Route path='/home' component={Index} />
            <Route path='/product/:id' component={ProductView}
                products={PRODUCTS} handleBuy={handleBuy} />
            <Route path='/cart' component={Cart}
                products={PRODUCTS} cart={cart} />
        </Route>
    </Router>
    , document.getElementById('content')
)