const React = require('react')

const Cart = function (props) {
    const cart = props.route.cart
    const products = props.route.products

    return (
        <div>
            <h1>Invoice</h1>
            <ul>
                {
                    Object.keys(cart).map((key, index) => {
                        return cart[key] ? (<li>{products[key].title}, quantity: {cart[key]}</li>) : null
                    })
                }
            </ul>
        </div>
    )

}

module.exports = Cart
