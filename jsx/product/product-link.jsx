const React = require('react')
const { Link } = require('react-router')

module.exports = function(props) {
    const styles = {
        width: '200px',
        height: '300px'
    }
    return (
        <Link style={styles} to={'/product/'+props.id}>
            <h2>{props.title}</h2>
            <img src={props.src}/>
        </Link>
    )
}