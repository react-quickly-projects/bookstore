const React = require('react')
const Modal = require('../modal.jsx')

class ProductView extends React.Component {

    handleBuy() {
        this.props.route.handleBuy(this.props.params.id)
        this.props.history.go(-1)
    }

    render() {
        const product = this.props.route.products[this.props.params.id]
        return (
            <Modal {...this.props}>
                <div>
                    <h1>{product.title}</h1>
                    <img src={product.src} />
                    <button className='btn btn-primary' onClick={this.handleBuy.bind(this)}>Buy</button>
                </div>
            </Modal>
        )

    }
}

module.exports = ProductView